# Desafio Xamarin forms ou WPF #

## 1– Layout ##

- [x] Criação da tela de histórico, funcionando em UWP e WPF
- [x] Nessa tela devem conter os campos, conforme print e renderizar tanto em UWP quanto em WPF utilizando Xamarin Forms. 

## 2 – Criação do MOCK ##

- [x] A tela deve sofrer alterações constantes semelhante a execução de ordens na bolsa, para nosso desafio devemos a cada 50 milisegundos incluir um item novo na lista e/ou modificar os itens existes. A tela deve refletir essa alteração. 
- [x] *Desafio extra: Simular momentos de aumento de carga, onde múltiplos itens da lista são alterados. 

## 3– Análise de Performance ##

- [x] A última parte do nosso desafio é analisar a performance da aplicação, fazendo um comparativo de UWP X WPF, principalmente ao uso de processamento e memória.

## 4 - Testes Unitários. ##

- [x] Você pode optar por usar um framework como o xUnit. Um diferencial é ter uma análise de cobertura de testes.