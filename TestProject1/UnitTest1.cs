using System;
using System.Collections.ObjectModel;
using System.Linq;
using Xunit;
using static App1.MainPage;

namespace TestProject1
{
    public class UnitTest1
    {
        private readonly Random rnd = new();

        [Fact]
        private void AdicionarOrdem_VerificarOrdens()
        {
            ObservableCollection<Ordem> ordens = new();
            int qtd = rnd.Next(500);
            int valor = rnd.Next(1000);
            double objetivo = valor * 1.2;
            ordens.Add(new Ordem { DataHora = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), Assessor = "-", Conta = "3934072", Ativo = "TF473", Tipo = "C", Qtd = qtd.ToString(), QtdAparente = "-", QtdDisp = "-", QtdCancel = "-", QtdExec = "0", Valor = valor.ToString(), ValorDisp = valor.ToString(), Objetivo = objetivo.ToString(), ObjDisp = objetivo.ToString(), Reducao = "-" });

            Assert.NotEmpty(ordens);
        }

        [Fact]
        private void AtualizarValorAleatorio_VerificarAumento() 
        {
            int[] valores = Enumerable.Range(1, 10).ToArray();
            int i = rnd.Next(10);
            int valorAnterior = valores[i];

            valores[i]++;

            Assert.True(valores[i] > valorAnterior);
        }
    }
}
