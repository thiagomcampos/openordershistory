﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using System.Timers;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            OrdemView.ItemsSource = ordens;

            aTimer.Elapsed += new ElapsedEventHandler(GerarOrdens);
            aTimer.Interval = 50;
            aTimer.Enabled = true;
        }

        public ObservableCollection<Ordem> ordens = new ObservableCollection<Ordem>();
        public Timer aTimer = new Timer();
        public Random rnd = new Random();

        private void GerarOrdens(object sender, ElapsedEventArgs e)
        {
            if (ordens.Count < 500)
            {
                Dispatcher.BeginInvokeOnMainThread(() =>
                {
                    AdicionarOrdem();
                    if (ordens.Count >= 300) // increase load after 300 items
                    {
                       AumentarCarga();
                    }
                });
            } else
            {
                aTimer.Dispose();
            }
        }

        private void AdicionarOrdem()
        {
            int qtd = rnd.Next(500);
            int valor = rnd.Next(1000);
            double objetivo = valor * 1.2;
            ordens.Add(new Ordem { DataHora = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), Assessor = "-", Conta = "3934072", Ativo = "TF473", Tipo = "C", Qtd = qtd.ToString(), QtdAparente = "-", QtdDisp = "-", QtdCancel = "-", QtdExec = "0", Valor = valor.ToString(), ValorDisp = valor.ToString(), Objetivo = objetivo.ToString(), ObjDisp = objetivo.ToString(), Reducao = "-" });
        }

        private void AumentarCarga()
        {
            int qtdeItensParaAlterar = rnd.Next(30);

            for (int i = 0; i < qtdeItensParaAlterar; i++)
            {
                AtualizarItemAleatorio();
            }
        }

        private void AtualizarItemAleatorio()
        {
            int itemASerAlterado = rnd.Next(ordens.Count);
            int qtd = short.Parse(ordens[itemASerAlterado].Qtd);
            int qtdExec = short.Parse(ordens[itemASerAlterado].QtdExec);
            if (qtdExec < qtd)
            {
                ordens[itemASerAlterado].QtdExec = rnd.Next(qtdExec, qtd+1).ToString();
                ordens[itemASerAlterado].DataHora = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public class Ordem : INotifyPropertyChanged
        {
            public string DataHora { get; set; }
            public string Assessor { get; set; }
            public string Conta { get; set; }
            public string Ativo { get; set; }
            public string Tipo { get; set; }
            public string Qtd { get; set; }
            public string QtdAparente { get; set; }
            public string QtdDisp { get; set; }
            public string QtdCancel { get; set; }
            public string QtdExec { get; set; }
            public string Valor { get; set; }
            public string ValorDisp { get; set; }
            public string Objetivo { get; set; }
            public string ObjDisp { get; set; }
            public string Reducao { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
        }
    }
}
